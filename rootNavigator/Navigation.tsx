import { Registration } from '../modules/Registration/View/Registration'
import React, { PureComponent } from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Loader } from "../modules/Loader/View/Loader";
import { ListScreen } from "../helpers/ListScreen";

const Stack = createStackNavigator()

export class Navigation extends PureComponent {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Registration"
          headerMode='none'
        >
          <Stack.Screen
            name={ListScreen.Login}
            component={Registration}
          />
          <Stack.Screen
            name={ListScreen.Loader}
            component={Loader}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
