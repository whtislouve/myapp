/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {Navigation,} from 'D:/AndroidProjects/Pr_1/MyApp/rootNavigator/Navigation.tsx';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Navigation);
