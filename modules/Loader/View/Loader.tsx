import React, { PureComponent } from 'react'
import {
  StyleSheet,
  View,
  Image,
} from 'react-native'

import { windowWidth } from '../../../helpers/dimension'
import { Color } from '../../../helpers/color'
import { ImageRepository } from '../../../helpers/ImageRepository'

export class Loader extends PureComponent {

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={ImageRepository.logo}
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignSelf: "center",
    marginTop: windowWidth * 0.78,
    marginBottom:  windowWidth * 0.77,
    marginLeft: windowWidth * 0.088,
    marginRight: windowWidth * 0.087
  },
  logo: {
    width: windowWidth * 0.82,
    height: windowWidth * 0.22,
    backgroundColor: Color.white
  },
});
