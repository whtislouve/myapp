import React, { PureComponent } from "react"
import {TouchableOpacity, TouchableOpacityProps, Text} from "react-native"

interface IProps extends TouchableOpacityProps{
  title: string

}

interface IState {

}

export class CustomButton extends PureComponent<IProps, IState>{
  render(){
    return(
      <TouchableOpacity
        {...this.props}
      >
        <Text>
          {this.props.title}
        </Text>
      </TouchableOpacity>
    )
  }
}
