import React, { PureComponent } from "react"
import { StyleSheet, TextStyle, TextInput, TextInputProps } from "react-native"
import { Color } from '../../../helpers/color'
import { windowWidth } from '../../../helpers/dimension'
import { style } from '../../../helpers/style'

interface IProps extends TextInputProps{
  label: string
  style: TextStyle

 }

 interface IState {

 }

export class CustomInput extends PureComponent<IProps, IState>{
  render(){
    const inputFlattenStyles = StyleSheet.flatten([
      inputStyles.cardInput,
      this.props.style

    ])
    return(
        <TextInput
          {...this.props}
          style={inputFlattenStyles}
        />
    )
  }
}

const inputStyles = StyleSheet.create({
  cardInput: style.text({
    borderWidth: 1,
    borderColor: Color.textInput,
    backgroundColor: 'red',
    fontSize: windowWidth * 0.04
  }),
})


