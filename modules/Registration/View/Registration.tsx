import React, { PureComponent } from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Alert,
  TouchableOpacity,
  TouchableOpacityProps,
  ScrollView
} from 'react-native'

import { windowWidth } from '../../../helpers/dimension'
import { Color } from '../../../helpers/color'
import { Fonts } from '../../../helpers/fonts'
import { Loader } from "../../Loader/View/Loader"
import { CustomButton } from "../../CustomButton/View/CustomButton"
import { CustomInput } from '../../CustomInput/View/CustomInput'
import { style } from '../../../helpers/style'
import axios from 'axios'
import {Simulate} from 'react-dom/test-utils'


interface IState {
  trainer: string
  posts: IPost[]
  isLoading: boolean
  dataInput: string
}

interface IPost {
  body: string
  id: number
  title: string
  userId: number
}

export class Registration extends PureComponent <IState> {

  state = {
    trainer: '',
    posts: [],
    isLoading: true,
    dataInput: ''
  }

  componentDidMount(): void {
    axios.get('http://jsonplaceholder.typicode.com/posts')
      .then(posts => {
        this.setState({posts: posts.data})
      })
      .catch(error => {
        Alert.alert('Ошибка', error)
      })
  }

  onPressHandler = () => {
    Alert.alert("Событие", this.state.dataInput)

  }

  onChangeText = (dataInput): void => {
    this.setState({dataInput})

  }

  render() {
    return (
      <View style={styles.mainConteiner}>
        <CustomButton
          title="look"
          onPress={this.onPressHandler}

        />
        <CustomInput
          label="big"
          value={this.state.dataInput}
          onChangeText={this.onChangeText}
          style={styles.card}
        />
          {this.state.posts.map(post => {
            return(
              <View key={Math.random().toString()}>
                <Text>
                  {post.id}
                </Text>
              </View>
            )
          })
          }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  mainConteiner: {
    flex: 1,
  },
  card: style.text({
    height: windowWidth * 0.07,
    width: windowWidth * 0.5,
  }),
  title: {
    height: windowWidth * 0.05,
    width: windowWidth * 0.5,
    marginTop: windowWidth * 0.03,
    marginLeft: windowWidth * 0.516,

  },
  loadingContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    backgroundColor: '#000'
  },
  loadingText: {
    color: Color.white
  },
  regWord: {
    alignSelf: "center",
    marginTop: windowWidth * 0.04,
    marginLeft: windowWidth * 0.31,
    marginRight: windowWidth * 0.31,
    fontFamily: "SF UI Display Thin",
    fontStyle: "normal",
    fontSize: windowWidth * 0.05
  },
  regRectangle: {
    width: windowWidth * 1,
    height: windowWidth * 0.14,
    borderColor: Color.textInput,
    borderWidth: 1,
  },
  idTrainerText: {
    marginTop: windowWidth * 0.06,
    marginLeft: windowWidth * 0.042,
    marginRight: windowWidth * 0.704,
    fontFamily: "SF UI Display Thin",
    fontStyle: "normal",
    fontSize: windowWidth * 0.03,
    color: Color.textIdTrainer,
  },
  enterYourNicknameText: {
    marginTop: windowWidth * 0.064,
    marginLeft: windowWidth * 0.042,
    marginRight: windowWidth * 0.586,
    fontFamily: Fonts.sfUiDisplay,
    fontStyle: "normal",
    fontSize: windowWidth * 0.03,
    color: Color.textIdTrainer
  },
  textInput: {
    marginLeft: windowWidth * 0.042,
    marginRight: windowWidth * 0.042,
    marginTop: windowWidth * 0.024,
    width: windowWidth * 0.914,
    height: windowWidth * 0.11,
    borderColor: Color.textInput,
    borderWidth: 1,
    borderRadius: windowWidth * 0.016
  },
  enterButton: {
    width: windowWidth * 0.914,
    height: windowWidth * 0.11,
    marginTop: windowWidth * 0.06,
    backgroundColor: Color.enterButton,
    borderRadius: windowWidth * 0.016
  },
  enterButtonText: {
    alignSelf: "center",
    fontFamily: Fonts.sfUiDisplay,
    fontStyle: "normal",
    fontSize: windowWidth * 0.04,
    marginTop: windowWidth * 0.029,
    color: Color.white
  }
});
