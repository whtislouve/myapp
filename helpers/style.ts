import { ViewStyle, TextStyle, ImageStyle } from 'react-native'

export const style = {
  view: (s: ViewStyle): ViewStyle => s,
  text: (s: TextStyle): TextStyle => s,
  image: (s: ImageStyle): ImageStyle => s,
}
