import { Dimensions, } from 'react-native'

const windowDimensions = Dimensions.get('window')

export const windowWidth = windowDimensions.width